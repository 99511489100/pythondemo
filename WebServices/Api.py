from flask import Flask, redirect, url_for, request
import json
from Entity.EntityClasses import Candidate
from Services.FetchDetails import fetch_candidate_details
app = Flask(__name__)

@app.route('/success/<dict>')
def success(dict):
   return  dict



@app.route('/Details',methods = ['POST', 'GET'])
def fetch_details():
   if request.method == 'POST':
      id = request.form['id']

      candidate = fetch_candidate_details(id)

      # j_data = json.dumps(candidate)

      jsonstr1 = json.dumps(candidate.__dict__)
      return redirect(url_for('success',dict = jsonstr1))
   else:
      pass
      # user = request.args.get('nm')
      # return redirect(url_for('success',name = user))



if __name__ == '__main__':
   app.run(debug = True)