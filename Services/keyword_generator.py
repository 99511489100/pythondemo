import pandas as pd
# importing modules
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

ps = PorterStemmer()

# Fetching search phrase
# What is a c program
# wanted java and python coders
sentence = input("Enter search phrase\n")
words = sentence.split(' ')
# print(words)

# Data Cleaning

# 1. Identify the root words and append to set to eliminate duplicates
root_set = []
for w in words:
    # print(w, " : ", ps.stem(w))
    root_set.append(ps.stem(w).lower())

# 2. Eliminating Duplicates
root_set = list(set(root_set))

# 3. Eliminate stop words

df = pd.read_excel('../Assets/stopwords.xlsx')
stop_words = list(df['stopwords'])

refined_list = []

for word in root_set:
    if word not in stop_words:
        refined_list.append(word)

print('Refined Keywords generated:')
print(refined_list)
