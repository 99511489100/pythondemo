from abc import ABC, abstractmethod

class Calculation(ABC):
    global input_no
    @abstractmethod
    def perform_calculation(input_no):
        pass
    def fetch_input(self):
        return input('Enter a value to perform a calculation\n')


class multiply(Calculation):
    def perform_calculation(self,input_no):
        print('Multiplication Result:' + str(input_no*input_no))

class Add(Calculation):
    def perform_calculation(self,input_no):
        print('Addition Result:' + str(input_no+input_no))

multiply_obj = multiply()
no = multiply_obj.fetch_input()
multiply_obj.perform_calculation(int(no))

Add_obj = Add()
no = Add_obj.fetch_input()
Add_obj.perform_calculation(int(no))



