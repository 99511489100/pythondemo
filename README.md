Project Description:
----------------------------
This is a demo project, created to implement various major python concepts.

Dependencies:
----------------------------

-- Python

-- Flask API

-- PostgreSQL

-- SourceTree (for BitBucket)


Some Major Modules used:
----------------------------

-- Pandas

-- Openpyxl

-- abc (Abstract Class)

-- psycopg2 (PostgreSQL)

-- nltk (Natural Language Toolkit for Stemming)

-- flask

-- json


Folder Description:
----------------------------
View: Contains HTML files.

WebServices: Contains APIs.

Entity: Contains the entity classes.

Services: Contains the service logic and database interaction functions.

Assets: Contains extra files like excel, csv, pdf, images etc


Sample Flow:
----------------------------
View -> WebServices -> Services

View <- WebServices <- Entity <- Services

Example flow:

Retrievedetails.html -> Api.py -> Services (Imported Entity class 'Candidate' and populated values from postgre DB) -> Api.py 

Demo concepts implemented (Sprint 1):
----------------------------
-- WebService Integration (Flask API)

-- Implemented End to End flow (View -> Logic -> DB and vice versia)

-- Used class object to pass data


Demo concepts implemented (Sprint 2):
----------------------------

-- Inheritance and Abstraction class Demo

-- Implemented stemming concept to trim the stop words, duplicates and gramitically repeated words


Next Sprint Implementations:
----------------------------

-- UI design (Angular, Bootstrap)